package com.wjs.boot.dao.master;

import com.wjs.boot.entity.User;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("masterUserMapper")
public interface UserMapper {
    int deleteByPrimaryKey(Long id);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

    List<User> selectUser();
}