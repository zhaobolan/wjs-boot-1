package com.wjs.boot.controller;


import javax.annotation.Resource;

import com.github.pagehelper.PageInfo;
import com.wjs.boot.entity.User;
import com.wjs.boot.service.UserService;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/user")
public class UserController {

    @Resource
    private UserService userService;

    @GetMapping(value = "/master/{id}")
    public User getUser(@PathVariable long id) throws Exception {
        return this.userService.getUserById(id);
    }


    @GetMapping(value = "/branch/{id}")
    public User getBranchUser(@PathVariable long id) throws Exception {
        return this.userService.getBranchUser(id);
    }

    @GetMapping(value="listUser")
    public PageInfo<User> listUser(
            @RequestParam(value="page", required=false, defaultValue="1") int page,
            @RequestParam(value="page-size", required=false, defaultValue="5") int pageSize){
        List<User> result = userService.listUser(page, pageSize);
        // PageInfo包装结果，返回更多分页相关信息
        PageInfo<User> pi = new PageInfo<User>(result);
        return pi;
    }

}
