package com.wjs.boot;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;

@SpringBootApplication
public class WjsBootApplication {


	private static Logger log = LoggerFactory.getLogger(WjsBootApplication.class);

	public static void main(String[] args) {
		ConfigurableApplicationContext application = SpringApplication.run(WjsBootApplication.class, args);
		Environment env = application.getEnvironment();
		log.info("\n----------------------------------------------------------\n\t" +
						"应用 '{}' 启动成功! 访问连接:\n\t" +
						"请访问连接: \t\thttp://{}:{}\n\t" +
						"----------------------------------------------------------",
				env.getProperty("spring.application.name"),
				"127.0.0.1",
				env.getProperty("server.port")
		);
	}
}
